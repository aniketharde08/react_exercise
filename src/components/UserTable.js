import React from 'react'

function UserTable (props) {
return(
  <div class="container">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Email</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {
          props.users.map( user => (
            <tr key={user.id}>
              <td>{user.id}</td>
              <td>{user.name}</td>
              <td>{user.email}</td>
              <td>
                <button type="button" class="btn btn-warning"
                  onClick={() => props.editRow(user)}
                >Edit</button>

                <button type="button" class="btn btn-danger"
                  onClick={() => props.deleteUser(user.id)}
                >Delete</button>
              </td>
            </tr>
          ))
        }
      </tbody>
    </table>
  </div>
  )}

export default UserTable
