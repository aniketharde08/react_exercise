import React, { useState } from 'react' 

function AddUserForm (props) {
	const initialFormState = { id: null, name: '', email: '' }
	const [ user, setUser ] = useState(initialFormState)

	const handleInputChange = event => {
		const { name, value } = event.target
		setUser({ ...user, [name]: value })
	}

	return (
		<div class="container">
			<div class="card">
		   		<div class="card-header">Add User</div>
				<div class="card-body">
					<form onSubmit={event => {
						event.preventDefault()
						if (!user.name || !user.email) return 
							props.addUser(user)
						
						setUser(initialFormState)
						}}
					>
						<input class="form-control" type="text" placeholder='ID' value={user.id} disabled={true}></input><br/>
						<input class="form-control" type="text" name="name" placeholder="Name" value={user.name} onChange={handleInputChange} /><br/>
						<input class="form-control" type="email" name="email" placeholder="Email" value={user.email} onChange={handleInputChange} />
						<button  class="btn btn-success">Submit</button>
					</form>
			    </div>
		    </div>
		</div>
	)
}

export default AddUserForm
