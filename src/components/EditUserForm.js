import React, { useState, useEffect } from 'react'

function EditUserForm (props) {
  const [ user, setUser ] = useState(props.currentUser)

  useEffect( () => {
      setUser(props.currentUser)
    },[ props ])

  const handleInputChange = event => {
    const { name, value } = event.target
    setUser({ ...user, [name]: value })
  }

  return (
    <div class="container">
			<div class="card">
        <div class="card-header">Edit User </div>
				<div class="card-body">
          <form onSubmit={event => {
              event.preventDefault()
                if (!user.name || !user.email) return 
                  props.updateUser(user.id, user)
            }}
          >
            <input class="form-control" type="text" placeholder='ID' value={user.id} disabled={true}></input><br/>
            <input class="form-control" type="text" name="name" value={user.name} onChange={handleInputChange} /><br/>
            <input class="form-control" type="email" name="email" value={user.email} onChange={handleInputChange} />
            <button class="btn btn-success">Update</button>
            <button class="btn btn-danger" onClick={() => props.setEditing(false)}>
              Cancel
            </button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default EditUserForm
