import React, { useState } from 'react'
import EditUserForm from './components/EditUserForm';
import AddUserForm from './components/AddUserForm';
import UserTable from './components/UserTable';
import './App.css';

function App () {
	const initialFormState = { id: null, name: '', email: '' }

	const [ users, setUsers ] = useState([])
	const [ currentUser, setCurrentUser ] = useState(initialFormState)
	const [ editing, setEditing ] = useState(false)

	const addUser = user => {
		user.id = users.length + 1
		setUsers([ ...users, user ])
	}

	const deleteUser = id => {
		setEditing(false)
		setUsers(users.filter(user => user.id !== id))
	}

	const updateUser = (id, updatedUser) => {
		setEditing(false)
		setUsers(users.map(user => (user.id === id ? updatedUser : user)))
	}

	const editRow = user => {
		setEditing(true)

		setCurrentUser({ id: user.id, name: user.name, email: user.email })
	}

	return (
		<div className="container">
				<div>
					{editing ? (
							<EditUserForm
								editing={editing}
								setEditing={setEditing}
								currentUser={currentUser}
								updateUser={updateUser}
							/>) : (<AddUserForm addUser={addUser} />)
					}
				</div>
				<div>
					<UserTable users={users} editRow={editRow} deleteUser={deleteUser} />
				</div>
			</div>
	)
}

export default App
